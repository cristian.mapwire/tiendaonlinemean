# TiendaOnlineMEAN

Consiste en una tienda con las funcionalidades de buscar un producto, agregarlo al carrito, eliminarlo del carrito, editar la cantidad de los productos en el carrito y finalmente simular un pago en el cual se envía el comprobante.